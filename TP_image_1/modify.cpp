 // modify.cpp

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLueY[250], cNomImgEcrite[250];
  int k, nH, nW, nTaille;
  
  if (argc != 4) 
     {
       printf("Usage: Y.pgm k ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLueY) ;
   sscanf(argv[2],"%d", &k);
   sscanf (argv[3],"%s",cNomImgEcrite);
   OCTET *ImgOut, *Y;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLueY, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(Y, OCTET, nTaille);
   lire_image_pgm(cNomImgLueY, Y, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);      
	
   for (int i=0; i < nH; i++)
      for (int j=0; j <  nW; j++)
     {
         if (Y[i*nW+j]+k<0) {ImgOut[i*nW+j]=0;}
         else if (Y[i*nW+j]+k>255) {ImgOut[i*nW+j]=255;}
         else {ImgOut[i*nW+j]=Y[i*nW+j]+k;}
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgOut); free(Y);
   return 1;
}