//amplification.cpp

#include <stdio.h>
#include "image_ppm.h"

void erosion (OCTET *ImgIn, OCTET *ImgOut, int nH, int nW){

 for (int i=1; i < nH-1; i++)
   for (int j=1; j < nW-1; j++)
     {
       if (ImgIn[i*nW+j]==255){
           if (ImgIn[i*nW+j+1]==0) {ImgOut[i*nW+j]=0;}
           else if (ImgIn[i*nW+j+1]==0) {ImgOut[i*nW+j]=0;}
           else if (ImgIn[(i+1)*nW+j]==0) {ImgOut[i*nW+j]=0;} 
           else if (ImgIn[i*nW+j-1]==0) {ImgOut[i*nW+j]=0;}
           else if (ImgIn[(i-1)*nW+j]==0) {ImgOut[i*nW+j]=0;}
           else if (ImgIn[(i+1)*nW+j+1]==0) {ImgOut[i*nW+j]=0;}
           else if (ImgIn[(i-1)*nW+j-1]==0) {ImgOut[i*nW+j]=0;}
           else if (ImgIn[(i+1)*nW+j-1]==0) {ImgOut[i*nW+j]=0;}
           else if (ImgIn[(i+1)*nW+j+1]==0) {ImgOut[i*nW+j]=0;}
           else {ImgOut[i*nW+j]=255;}
       }
       else {ImgOut[i*nW+j]=0;}
     }
  }

void dilatation (OCTET *ImgIn, OCTET *ImgOut, int nH, int nW) {

 for (int i=1; i < nH-1; i++)
   for (int j=1; j < nW-1; j++)
     {
       if (ImgIn[i*nW+j]==0){
           if (ImgIn[i*nW+j+1]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[i*nW+j+1]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[(i+1)*nW+j]==255) {ImgOut[i*nW+j]=255;} 
           else if (ImgIn[i*nW+j-1]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[(i-1)*nW+j]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[(i+1)*nW+j+1]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[(i-1)*nW+j-1]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[(i+1)*nW+j-1]==255) {ImgOut[i*nW+j]=255;}
           else if (ImgIn[(i+1)*nW+j+1]==255) {ImgOut[i*nW+j]=255;}
           else {ImgOut[i*nW+j]=0;}
       }
       else {ImgOut[i*nW+j]=255;}

     }
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut, *ImgTest, *ImgTest1, *ImgTest2, *ImgTest3, *ImgTest4, *ImgTest5, *ImgTest6, *ImgTest7, *ImgTest8, *ImgTest9, *ImgTest10;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   allocation_tableau(ImgTest, OCTET, nTaille);
   allocation_tableau(ImgTest1, OCTET, nTaille);
   allocation_tableau(ImgTest2, OCTET, nTaille);
   allocation_tableau(ImgTest3, OCTET, nTaille);
   allocation_tableau(ImgTest4, OCTET, nTaille);
   allocation_tableau(ImgTest5, OCTET, nTaille);
   allocation_tableau(ImgTest6, OCTET, nTaille);
   allocation_tableau(ImgTest7, OCTET, nTaille);
   allocation_tableau(ImgTest8, OCTET, nTaille);
   allocation_tableau(ImgTest9, OCTET, nTaille);
   allocation_tableau(ImgTest10, OCTET, nTaille);

      
    dilatation (ImgIn, ImgTest, nH, nW);
    dilatation (ImgTest, ImgTest1, nH, nW);
    dilatation (ImgTest1, ImgTest2, nH, nW);
    erosion (ImgTest2, ImgTest3, nH, nW);
    erosion (ImgTest3, ImgTest4, nH, nW);
    erosion (ImgTest4, ImgTest5, nH, nW);
    erosion (ImgTest5, ImgTest6, nH, nW);
    erosion (ImgTest6, ImgTest7, nH, nW);
    erosion (ImgTest7, ImgTest8, nH, nW);
    dilatation (ImgTest8, ImgTest9, nH, nW);
    dilatation (ImgTest9, ImgTest10, nH, nW);
    dilatation (ImgTest10, ImgOut, nH, nW);


   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free (ImgTest); free (ImgTest1); free (ImgTest2); free (ImgTest3);
   free (ImgTest4); free (ImgTest5); free (ImgTest6); free (ImgTest7);
   free (ImgTest8); free (ImgTest9); free (ImgTest10); free(ImgOut);

   return 1;
}
