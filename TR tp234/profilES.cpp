#include <stdio.h>
#include "image_ppm.h"
#include <fstream>

/*Pour réaliser la question 2)b je cree un programme profil entrée sortie , je lui donne en argv[1]
l'image d'entrée et argv[2] l'image de sortie et ensuite lindice dela ligne   */

/*Arguments : nom_de_image ligne_ou_colonne indice_ligne_ou_colonne*/
int main(int argc, char* argv[])
{
  char cNomImgLue1[250];
  char cNomImgLue2[250];
  int nH,nW,nTaille;
  int i;

  
  if (argc != 4) 
     {
       printf("Usage: %s imageEntree.pgm imagesortie.pgm indiceLigne  \n",argv[0]  ); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue1) ;
   sscanf (argv[2],"%s",cNomImgLue2);
   sscanf (argv[3],"%d",&i);
   
   

   OCTET *ImgIn1 ,*ImgIn2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
   nTaille = nH * nW;
  
  //lecture et allocation de l'image d'entrée 
   allocation_tableau(ImgIn1, OCTET, nTaille);
   lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);

  //lecture et allocation de l'image de sortie
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);

  
      //vu que je traite une ligne ,la taille du tableau va être la largeur 

      int tab1[nW];
      int tab2[nW]; 

      for (int j=0; j<nW ; j++)
	    {
	      tab1[j] = ImgIn1[i*nW+j];
          tab2[j]= ImgIn2[i*nW+j];  

	      printf("%i \t : \t %i\t %i  \n", j, tab1[j] ,tab2[j] ) ;
	    }
      std::ofstream fichier("profilES.dat");

      for (int k=0; k<nW; k++){
	      fichier <<k<<"\t"<<tab1[k]<<"\t"<<tab2[k]<<"\n";
      }
      fichier.close();
    
  
}