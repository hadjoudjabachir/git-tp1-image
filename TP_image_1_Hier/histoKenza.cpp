#include <stdio.h>
#include<iostream>
using namespace std;


#include "image_ppm.h"

/*on lui donne l'image qu'il doit lire */
FILE* f;
int main(int argc, char* argv[])
{
  
  char cNomImgLue[250];
  char cNomImgEcrite[250]; 
  int nH, nW, nTaille;
  
  if (argc != 3) 
  {
    printf("Usage: %s chemin_image_lue chemin_image_écrite\n",argv[00] ); 
    exit (1) ;
  }

  sscanf (argv[1],"%s",cNomImgLue) ;//chemin vers l'image dont on veut l'histogramme 
  sscanf (argv[2] ,"%s" ,cNomImgEcrite );
  /*on lit l'image en entrée pour instancier son height ,width et ainsi sa taille en general */
  OCTET *ImgIn;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;

  
  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

  
  //histogramme d'une image en niveau de gris 
  int histo[256]; 
  for(int l=0 ; l<256; l++)
    {
       histo[l] = 0;
    } 

    for (int i=0; i < nH; i++){
        for (int j=0; j < nW; j++){
          //la on est dans le pixel courant 
          histo[ImgIn[i*nW+j]]++; 
          
        }
    }
            

    if((f=fopen(cNomImgEcrite,"w"))==NULL){
      printf("on a une erreur lors de l'ouverture de %s...",cNomImgEcrite);
      exit(EXIT_FAILURE);
    }
    for (int k=0;k<256;k++)
    {
      printf ("%d\n ",histo[k] );
      
       fprintf(f,"%d\t%d\n",k,histo[k]);
    } 

  fclose(f);

  free(ImgIn);
} 
