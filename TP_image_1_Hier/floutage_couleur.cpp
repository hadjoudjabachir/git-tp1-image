// floutage_couleur.cpp 

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
   int nW3 = nW *3; 
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);
	

  /* for (int i=0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       ImgOut[i]=(nR+ImgIn[i+(3*nW)]+ImgIn[i-(3*nW)]+ImgIn[i+3]+ImgIn[i-3])/5;
       ImgOut[i+1]=(nG+ImgIn[(i+1)+(3*nW)]+ImgIn[(i+1)-(3*nW)]+ImgIn[(i+1)+3]+ImgIn[(i+1)-3])/5;
       ImgOut[i+2]=(nB+ImgIn[(i+2)+(3*nW)]+ImgIn[(i-2)-(3*nW)]+ImgIn[(i+2)+3]+ImgIn[(i+2)-3])/5;
    }

*/

      for (int i=0; i < nH; i++) { 
        for (int j=0; j < nW3; j+=3) {
            if (i==0 || i==nH-1 || j==0 || j==nW3-4) { // bord, on recopie
                ImgOut[i*nW3+j] = ImgIn[i*nW3+j];
                ImgOut[i*nW3+j+1] = ImgIn[i*nW3+j+1];
                ImgOut[i*nW3+j+2] = ImgIn[i*nW3+j+2];
            } else {
                int sR = 0;
                int sV = 0;
                int sB = 0;
                for(int a=-1; a<2; a++) {
                    for(int b=-1; b<2; b++) {
                        sR += ImgIn[(i+a)*nW3 +j+ 3*b];
                        sV += ImgIn[(i+a)*nW3 +j+1+ 3*b];
                        sB += ImgIn[(i+a)*nW3 +j+2+ 3*b];
                    }
                }
                ImgOut[i*nW3+j] = sR/9;
                ImgOut[i*nW3+j+1] = sV/9;
                ImgOut[i*nW3+j+2] = sB/9;
            }
        }
     } 

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
   return 1;
}
