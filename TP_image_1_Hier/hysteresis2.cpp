// hystersis2.cpp

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, SB, SH;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm SeuilB SeuilH \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&SB);
   sscanf (argv[4],"%d",&SH);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
 for (int i=1; i < nH-1; i++)
   for (int j=1; j < nW-1; j++)
     {
          bool trouver = false;
          for (int _i=-1; _i <=1; _i++)
            for (int _j=-1; _j <=1; _j++){
              if (ImgIn[(i+_i)*nW+(j+_j)]==255) trouver = true;
          }
         if (ImgIn[i*nW+j]== 0) ImgOut[i*nW+j]=0;
         else if (ImgIn[i*nW+j]==255) ImgOut[i*nW+j]=255;
         if ((ImgIn[i*nW+j] > SB) && (ImgIn[i*nW+j]<SH) && trouver){ImgOut[i*nW+j]=255;}
         else {ImgOut[i*nW+j]=0;}
     }
     /* for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
         if (ImgIn[i*nW+j]== 0) ImgOut[i*nW+j]=0;
         else if (ImgIn[i*nW+j]==255) ImgOut[i*nW+j]=255;
         else if ((ImgIn[i*nW+j] >= SB) && (ImgIn[i*nW+j]<SH)){
           if (ImgIn[i*nW+j+1]==255) {ImgOut[i*nW+j]=255;}
           else if  (ImgIn[i*nW+j+1]==255) {ImgOut[i*nW+j]=255;}
           else if  (ImgIn[(i+1)*nW+j]==255) {ImgOut[i*nW+j]=255;} 
           else if  (ImgIn[i*nW+j-1]==255) {ImgOut[i*nW+j]=255;}
           else if  (ImgIn[(i-1)*nW+j]==255) {ImgOut[i*nW+j]=255;}
           else if  (ImgIn[(i+1)*nW+j+1]==255) {ImgOut[i*nW+j]=255;}
           else if  (ImgIn[(i-1)*nW+j-1]==255) {ImgOut[i*nW+j]=255;}
           else if  (ImgIn[(i+1)*nW+j-1]==255) {ImgOut[i*nW+j]=255;}
           else if  (ImgIn[(i+1)*nW+j+1]==255) {ImgOut[i*nW+j]=255;}
           else {ImgOut[i*nW+j]=0;}
     }
           else {ImgOut[i*nW+j]=255;}
}
*/
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
