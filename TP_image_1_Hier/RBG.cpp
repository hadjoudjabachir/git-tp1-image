 // RBG.cpp

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLueY[250], cNomImgLueCr[250], cNomImgLueCb[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB, R, G, B;
  
  if (argc != 5) 
     {
       printf("Usage: Y.pgm Cb.pgm Cr.pgm ImageOut.ppm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLueY) ;
   sscanf (argv[2],"%s",cNomImgLueCb);
   sscanf (argv[3],"%s",cNomImgLueCr);
   sscanf (argv[4],"%s",cNomImgEcrite);
   OCTET *ImgOut, *Y, *Cb, *Cr;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLueY, &nH, &nW);
   lire_nb_lignes_colonnes_image_pgm(cNomImgLueCb, &nH, &nW);
   lire_nb_lignes_colonnes_image_pgm(cNomImgLueCr, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(Y, OCTET, nTaille);
   allocation_tableau(Cb, OCTET, nTaille);
   allocation_tableau(Cr, OCTET, nTaille);
   lire_image_pgm(cNomImgLueY, Y, nH * nW);
   lire_image_pgm(cNomImgLueCb, Cb, nH * nW);
   lire_image_pgm(cNomImgLueCr, Cr, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);     

	   for (int i=0; i < nH; i++)
      for (int j=0; j <  nW; j++)
     {
      R=Y[i*nW+j]+1.402*(Cr[i*nW+j]-128);
      G=Y[i*nW+j]-0.34414*(Cb[i*nW+j]-128)-0.714414*(Cr[i*nW+j]-128);
      B=Y[i*nW+j]+1.772*(Cb[i*nW+j]-128);
      if (R<0) R = 0;
      if (R>255) R = 255;
      if (G<0) G = 0;
      if (G>255) G = 255;
      if (B<0) B = 0;
      if (B>255) B = 255;
      ImgOut[3*(i*nW+j)]=R;
      ImgOut[3*(i*nW+j)+1]=B;
      ImgOut[3*(i*nW+j)+2]=G;
     }

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgOut); free(Y);free(Cb); free(Cr);
   return 1;
}