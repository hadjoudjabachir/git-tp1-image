// RGBtoYCbCr.cpp

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250], cNomImgEcrite2[250], cNomImgEcrite3[250];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.ppm Y.pgm Cb.pgm Cr.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%s",cNomImgEcrite2);
   sscanf (argv[4],"%s",cNomImgEcrite3);
   OCTET *ImgIn, *Y, *Cb, *Cr;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(Y, OCTET, nTaille);
   allocation_tableau(Cb, OCTET, nTaille);
   allocation_tableau(Cr, OCTET, nTaille);
	
   for (int i=0; i < nTaille3; i+=3)
      {
        nR = ImgIn[i];
        nG = ImgIn[i+1];
        nB = ImgIn[i+2];
        Y[i/3]=(0.299*nR)+(0.587*nG)+(0.114*nB);
        Cb[i/3]=-0.1687*nR - 0.3313*nG + 0.5*nB + 128;
        Cr[i/3]=0.5*nR - 0.418*nG - 0.0813*nB + 128;
          if (Y[i/3] < 0) Y[i/3] = 0;
          if (Y[i/3] > 255) Y[i/3] = 255;
          if (Cb[i/3] < 0) Cb[i/3] = 0;
          if (Cb[i/3] > 255) Cb[i/3] = 255;
          if (Cr[i/3] < 0) Cr[i/3] = 0;
          if (Cr[i/3] > 255) Cr[i/3] = 255;

      }

   ecrire_image_pgm(cNomImgEcrite, Y,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite2, Cb,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite3, Cr,  nH, nW);
   free(ImgIn); free(Y);free(Cb); free(Cr);
   return 1;
}
