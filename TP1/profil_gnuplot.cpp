// profil_gnuplot.cpp

#include <stdio.h>
#include "image_ppm.h"
#include <fstream>
#include <iostream>

int main(int argc, char* argv[])
{
  char cNomImgLue[250], LigneOuColonne [250];
  int nH, nW, nTaille, indice;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pg \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",LigneOuColonne);
   sscanf (argv[3],"%d",&indice);
   std::ofstream fichier{"profil.dat"};
   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }

if (strcmp("C", LigneOuColonne)==0) {
  int* tableau = new int [nH];
  for (int i=0; i<nH; i++){
	  tableau[i]=ImgIn[i*nW+indice]; 
    fichier <<i<< "\t"<<tableau[i]<<"\n";

  }
}
if (strcmp("L", LigneOuColonne)==0) {
  int* tableau = new int [nW];
  for (int i=0; i<nW; i++){
	  tableau[i]=ImgIn[indice*nW+i]; 
    fichier <<i<< "\t"<<tableau[i]<<"\n";

  }
}
   free(ImgIn);

   return 1;
}
