#include <stdio.h>
#include <fstream>
#include "image_ppm.h"
int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 2) 
     {
       printf("Usage: ImageIn.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;

   int tableauR [256];
   for (int i=0; i<256; i++){
      tableauR[i]=0;
   }
   int tableauG [256];
   for (int i=0; i<256; i++){
      tableauG[i]=0;
   }
   int tableauB [256];
   for (int i=0; i<256; i++){
      tableauB[i]=0;
   }
   OCTET *ImgIn, *ImgOut;
   
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);
	

for (int i=0; i < nTaille3; i+=3){
    nR = ImgIn[i];
    nG = ImgIn[i+1];
    nB = ImgIn[i+2];

    tableauR[nR]++;
    tableauG[nG]++;
    tableauB[nB]++;
 }

std::ofstream fichier{"histo3.dat"};

for (int i=0; i<256; i++){
   printf("%i %i %i %i\n",i,tableauR[i],tableauG[i],tableauB[i]);
	fichier <<i<< "\t"<<tableauR[i]<< "\t"<<tableauG[i]<< "\t"<<tableauB[i]<<"\n";
}
free(ImgIn);
}

